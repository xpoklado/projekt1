#include "Dopravce.h"
#include <iostream>
using namespace std;


class Mesto {

private:
	string m_nazevMesta;

public:
	Mesto(string nazevMesta);

	string getNazevPolohy();

	Dopravce* JdiTam();
};
