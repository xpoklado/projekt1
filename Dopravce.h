#include "Zbozi.h"
#include <iostream>
#include <array>
using namespace std;

class Dopravce {

private:
	string m_jmeno;
	int m_zivot;
	int m_obrana;
	vektor<Zbozi*> m_zbozi;
	float m_penize;

public:
Dopravce(string jmeno, int zivot, int obrana, int penize);

float getPenize();

string getJmeno();

int getZivot();

int getObrana();

void nakupZbozi(Zbozi* zbozi);

void prodejZbozi(Zbozi* zbozi);

void vypisInfo();
};
