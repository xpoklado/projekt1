#include "Mesto.h"
#include "Dopravce.h"
#include <iostream>
#include <array>
using namespace std;

class Cesta {

private:
	vektor<Mesto*> m_zmenaMesta;
	Cesta* s_Cesta;

public:
 Cesta();

 Cesta* getCesta();

void CestaSmerem(Mesto* nazevMesta);

void vypisRejesrik(Dopravce* jmeno);

~Cesta();
};
